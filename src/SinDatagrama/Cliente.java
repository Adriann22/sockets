package SinDatagrama;

import java.io.*;
import java.net.*;

public class Cliente {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 50005)) {
            try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {
                try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                    try (BufferedReader buf = new BufferedReader(new InputStreamReader(System.in))) {

                        String servidor;

                        while ((servidor = in.readLine()) != null) {
                            System.out.println("Servidor: " + servidor);
                            if (servidor.equals("Adiós."))
                                break;
                            System.out.print("Cliente: ");
                            String userInput = buf.readLine();
                            out.println(userInput);
                            if (userInput.equals("salir")) {
                                break;
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


