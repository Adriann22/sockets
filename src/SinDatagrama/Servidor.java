package SinDatagrama;

import java.io.*;
import java.net.*;

public class Servidor {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(50005)) {
            System.out.println("esperando conexion");
            while (true) {
                Socket clienteSocket = serverSocket.accept();
                System.out.println("Cliente conectado");
                new Thread(new ServicioCliente(clienteSocket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
class ServicioCliente implements Runnable {
    public Socket clienteSocket;
    public ServicioCliente(Socket clienteSocket) {
        this.clienteSocket = clienteSocket;
    }
    public void run() {
        try (
                Socket socket = clienteSocket;
                OutputStream outputStream = socket.getOutputStream();
                InputStream inputStream = socket.getInputStream();
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
                PrintWriter out = new PrintWriter(bufferedWriter, true);
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                BufferedReader in = new BufferedReader(bufferedReader);
        ) {
            out.println("Hola , introduce 'salir' para salir.");
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                out.println("Dime el primer número:");
                double numero1 = Double.parseDouble(in.readLine());
                out.println("Dime el segundo número:");
                double numero2 = Double.parseDouble(in.readLine());
                out.println("Dime la operacion");
                String operador = in.readLine();
                double resultado = 0;
                switch (operador) {
                    case "+":
                        resultado = numero1 + numero2;
                        break;
                    case "-":
                        resultado = numero1 - numero2;
                        break;
                    case "*":
                        resultado = numero1 * numero2;
                        break;
                    case "/":
                        resultado = numero1 / numero2;
                        break;
                }
                out.println("El resultado es: " + resultado);
            }
            clienteSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

