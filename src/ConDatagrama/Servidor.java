package ConDatagrama;

import java.net.*;

public class Servidor {
    public static void main(String[] args) {
        try (DatagramSocket socket = new DatagramSocket(50005)) {
            byte[] receiveBuffer = new byte[1024];
            System.out.println("Esperando conexion");
            while (true) {
                DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                socket.receive(receivePacket);
                InetAddress clientAddress = receivePacket.getAddress();
                int clientPort = receivePacket.getPort();
                new Thread(() -> servir(socket, clientAddress, clientPort)).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void servir(DatagramSocket socket, InetAddress clientAddress, int clientPort) {
        try {
            double numero1 = 10;
            double numero2 = 5;
            String operador = "+";

            double resultado = calcularResultado(numero1, numero2, operador);

            byte[] sendData;
            String message = String.valueOf(resultado);
            sendData = message.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, clientAddress, clientPort);
            socket.send(sendPacket);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static double calcularResultado(double numero1, double numero2, String operador) {
        switch (operador) {
            case "+":
                return numero1 + numero2;
            case "-":
                return numero1 - numero2;
            case "*":
                return numero1 * numero2;
            case "/":
                return numero1 / numero2;
            default:
                System.out.println("Operador no válido: " + operador);
                return -1;
        }
    }
}

